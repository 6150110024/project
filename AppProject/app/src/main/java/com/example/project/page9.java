package com.example.project;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.zxing.WriterException;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

public class page9 extends AppCompatActivity {
    String TAG="GenerateQrCode";
    TextView edttxt;
    ImageView qrimg;
    String inputvalue;
    Button start;
    Bitmap bitmap;
    QRGEncoder qrgEncoder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page9);
        qrimg = (ImageView)findViewById(R.id.qrcode);
        edttxt=(TextView) findViewById(R.id.edittext);
        start=(Button)findViewById(R.id.createbtn);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputvalue=edttxt.getText().toString().trim();
                if (inputvalue.length()>0){
                    WindowManager manager = (WindowManager)getSystemService(WINDOW_SERVICE);
                    Display display = manager.getDefaultDisplay();
                    Point point = new Point();
                    display.getSize(point);
                    int width=point.x;
                    int height=point.y;
                    int smallerdimension=width<height ? width:height;
                    smallerdimension=smallerdimension*3/4;
                    qrgEncoder=new QRGEncoder(inputvalue,null, QRGContents.Type.TEXT,smallerdimension);
                    try {
                        bitmap=qrgEncoder.encodeAsBitmap();
                        qrimg.setImageBitmap(bitmap);
                    }
                    catch (WriterException e) {
                        Log.v(TAG,e.toString());
                    }
                }else {
                    edttxt.setError("Require");
                }
            }
        });

        ImageView imageView1;
        imageView1 = (ImageView) findViewById(R.id.imageView14);
        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(page9.this, page4.class);
                startActivity(i);
            }
        });
    }
}