package com.example.project;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class page2 extends AppCompatActivity {

    private static String TAG_RETROFIT_GET_POST = "RETROFIT_GET_POST";
    EditText cusIDEditText,cusNameEditText,emailEditText,cusPasswordEditText,cusAddressEditText,phoneEditText;
    Button registerButton = null;
    private ProgressDialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page2);
        initControls();
        /* When register user account button is clicked. */
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                final String cusIdValue = cusIDEditText.getText().toString();
                String cusNameValue = cusNameEditText.getText().toString();
                final String cusEmailValue = emailEditText.getText().toString();
                final String cusPasswordValue = cusPasswordEditText.getText().toString();
                final String cusAddressValue = cusAddressEditText.getText().toString();
                final String cusPhoneValue = phoneEditText.getText().toString();

                // 1. Building retrofit object
                Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(Registers.BASE_URL).addConverterFactory(GsonConverterFactory.create())
                                    .build();
                //Create instance for user manager interface and return it.
                Registers registerInterface = retrofit.create(Registers.class);
                // Use default converter factory, so parse response body text took http3.ResponseBody object.
                Call<ResponseBody> call = registerInterface.registers(cusIdValue,cusNameValue, cusEmailValue,cusPasswordValue, cusAddressValue, cusPhoneValue);
                // Create a Callback object, because we do not set JSON converter, so the return object is ResponseBody be default.
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call,
                                           Response<ResponseBody> response) {
                        hideProgressDialog();
                        StringBuffer messageBuffer = new StringBuffer();
                        int statusCode = response.code();
                        if(statusCode == 200)
                        {
                            try {
                                // Get return string.
                                String returnBodyText = response.body().string();
                                // Because return text is a json format string, so we should parse it manually.
                                Gson gson = new Gson();
                                TypeToken<List<RegisterResponse>> typeToken = new
                                        TypeToken<List<RegisterResponse>>(){};
                                // Get the response data list from JSON string.
                                List<RegisterResponse> registerResponseList =
                                        gson.fromJson(returnBodyText, typeToken.getType());
                                if(registerResponseList!=null &&
                                        registerResponseList.size() > 0) {
                                    RegisterResponse registerResponse =
                                            registerResponseList.get(0);
                                    if (registerResponse.isSuccess()) {

                                        messageBuffer.append(registerResponse.getMessage());
                                    } else {
                                        messageBuffer.append("User register failed.");
                                    }
                                }
                            }catch(IOException ex)
                            {
                                Log.e(TAG_RETROFIT_GET_POST, ex.getMessage());
                            }
                        }else
                        {
                            // If server return error.
                            messageBuffer.append("Server return error code is ");
                            messageBuffer.append(statusCode);
                            messageBuffer.append("\r\n\r\n");
                            messageBuffer.append("Error message is ");
                            messageBuffer.append(response.message());
                        }
                        // Show a Toast message.
                        Toast.makeText(getApplicationContext(),
                                messageBuffer.toString(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        hideProgressDialog();
                        Toast.makeText(getApplicationContext(), t.getMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
    /* Initialize all UI controls. */
    private void initControls()
    {
        if(cusIDEditText==null)
        {
            cusIDEditText = (EditText)findViewById(R.id.custID);
        }
        if(cusNameEditText==null)
        {
            cusNameEditText = (EditText)findViewById(R.id.custname);
        }
        if(emailEditText==null)
        {
            emailEditText = (EditText)findViewById(R.id.custemail);
        }
        if(cusPasswordEditText==null)
        {
            cusPasswordEditText = (EditText)findViewById(R.id.password);
        }
        if(cusAddressEditText==null)
        {
            cusAddressEditText = (EditText)findViewById(R.id.address);
        }
        if(phoneEditText==null)
        {
            phoneEditText = (EditText)findViewById(R.id.phone);
        }
        if(registerButton == null)
        {
            registerButton = (Button)findViewById(R.id.button3);
        }
        if(progressDialog == null) {
            progressDialog = new ProgressDialog(page2.this);
        }
    }
    /* Show progress dialog. */
    private void showProgressDialog()
    {
        // Set progress dialog display message.
        progressDialog.setMessage("Please Wait");
        // The progress dialog can not be cancelled.
        progressDialog.setCancelable(false);
        // Show it.
        progressDialog.show();
    }
    /* Hide progress dialog. */
    private void hideProgressDialog()
    {
        // Close it.
        progressDialog.hide();
    }
}




//